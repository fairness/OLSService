package pharma.Term;


import org.neo4j.ogm.annotation.NodeEntity;


@NodeEntity
/**
 * NcbiTaxon Term class
 * (no difference from base)
 * @author asztrik
 *
 */
public class NcbiTaxonTerm extends AbstractTerm {

	
}
