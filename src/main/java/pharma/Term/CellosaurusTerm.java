package pharma.Term;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
/**
 * Cellosaurus Term class
 * Extended with Cellosaurus specific fields
 * @author asztrik
 *
 */
public class CellosaurusTerm extends AbstractTerm {

	private String accessionNumber;
	
	private String cellLineName;

	public String getAccessionNumber() {
		return accessionNumber;
	}

	public String getCellLineName() {
		return cellLineName;
	}

	public void setCellLineName(String cellLineName) {
		this.cellLineName = cellLineName;
	}

	public void setAccessionNumber(String accessionNumber) {
		this.accessionNumber = accessionNumber;
	} 

public JSONObject toJSON() {
		
		// create base JSON-LD
		JSONObject output = new JSONObject();
		
		// add type, fixed
		output.put("@type", "skos:Concept");
		
		// add an ID, which is the IRI of the term
		JSONArray iriWrapperArray = new JSONArray();
		iriWrapperArray.put(this.iri);
		output.put("skos:exactMatch", iriWrapperArray);
		
		// sub-object for storing the label + language info
		JSONObject labelObject = new JSONObject();
		JSONArray labelWrapperArray = new JSONArray();
		labelObject.put("@value", this.label);
		labelObject.put("@language", "eng");
		labelWrapperArray.put(labelObject);
		
		output.put("skos:prefLabel", labelWrapperArray);
		
		if(this.cellLineName != null && !this.cellLineName.isEmpty()) {
			output.put("cellLineName", this.cellLineName);
		}
		
		if(this.accessionNumber != null && !this.accessionNumber.isEmpty()) {
			output.put("accessionNumber", this.accessionNumber);
		}
		
		// sub-object for storing the label + language info		
		if(this.synonym != null && !this.synonym.isEmpty()) {
			
			JSONArray altLabelArray = null;
			
			try {
				//synonyms were stored as an array of objects
				altLabelArray = new JSONArray(this.synonym);
			} catch(JSONException je) {
				// synonym was stored as a single element, not an array
				altLabelArray = new JSONArray();
				altLabelArray.put(this.synonym);
			}
			
			
			
			output.put("skos:altLabel", altLabelArray);
		}
		
		if(this.ancestors != null && !this.ancestors.isEmpty()) {
			output.put("@hierarchicalAncestors", this.ancestors);
		}
		
		return output;
	}
	
}
