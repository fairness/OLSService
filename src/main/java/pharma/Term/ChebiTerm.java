package pharma.Term;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
/**
 * Chebi Term class
 * Inchi and InchiKey
 * @author asztrik
 *
 */
public class ChebiTerm extends AbstractTerm {

	protected String inchi;

	protected String inchiKey;
	
	public String getInchi() {
		return inchi;
	}

	public void setInchi(String inchi) {
		this.inchi = inchi;
	}

	public String getInchiKey() {
		return inchiKey;
	}

	public void setInchiKey(String inchiKey) {
		this.inchiKey = inchiKey;
	}

public JSONObject toJSON() {
		
		// create base JSON-LD
		JSONObject output = new JSONObject();
		
		// add type, fixed
		output.put("@type", "skos:Concept");
		
		// add an ID, which is the IRI of the term
		JSONArray iriWrapperArray = new JSONArray();
		iriWrapperArray.put(this.iri);
		output.put("skos:exactMatch", iriWrapperArray);
		
		// sub-object for storing the label + language info
		JSONObject labelObject = new JSONObject();
		JSONArray labelWrapperArray = new JSONArray();
		labelObject.put("@value", this.label);
		labelObject.put("@language", "eng");
		labelWrapperArray.put(labelObject);
		
		output.put("skos:prefLabel", labelWrapperArray);
		
		if(this.inchi != null && !this.inchi.isEmpty()) {
			output.put("inchi", this.inchi);
		}
		
		if(this.inchiKey != null && !this.inchiKey.isEmpty()) {
			output.put("inchiKey", this.inchiKey);
		}
		
		// sub-object for storing the label + language info		
		if(this.synonym != null && !this.synonym.isEmpty()) {
			
			JSONArray altLabelArray = null;
			
			try {
				//synonyms were stored as an array of objects
				altLabelArray = new JSONArray(this.synonym);
			} catch(JSONException je) {
				// synonym was stored as a single element, not an array
				altLabelArray = new JSONArray();
				altLabelArray.put(this.synonym);
			}
			
			
			
			output.put("skos:altLabel", altLabelArray);
		}
		
		if(this.ancestors != null && !this.ancestors.isEmpty()) {
			output.put("@hierarchicalAncestors", this.ancestors);
		}
		
		return output;
	}
	

}
