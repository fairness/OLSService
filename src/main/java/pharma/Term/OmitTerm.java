package pharma.Term;
import org.neo4j.ogm.annotation.NodeEntity;



@NodeEntity
/**
 * Omit Term class
 * (no difference from base)
 * @author asztrik
 *
 */
public class OmitTerm extends AbstractTerm {

}
