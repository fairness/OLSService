package pharma.Term;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
/**
 * Uniprot Term class
 * Added uniprot-specific fields
 * @author asztrik
 *
 */
public class UniprotTerm extends AbstractTerm {
	
	
	protected String uniprotAccession; 
	protected String uniprotName;
	protected String proteinRecommendedName; 
	protected String proteinAlternativeName;
	protected String geneName;
	protected String organismName;
	
	public String getUniprotAccession() {
		return uniprotAccession;
	}

	public void setUniprotAccession(String uniprotAccession) {
		this.uniprotAccession = uniprotAccession;
	}

	public String getUniprotName() {
		return uniprotName;
	}

	public void setUniprotName(String uniprotName) {
		this.uniprotName = uniprotName;
	}

	public String getProteinRecommendedName() {
		return proteinRecommendedName;
	}

	public void setProteinRecommendedName(String proteinRecommendedName) {
		this.proteinRecommendedName = proteinRecommendedName;
	}

	public String getProteinAlternativeName() {
		return proteinAlternativeName;
	}

	public void setProteinAlternativeName(String proteinAlternativeName) {
		this.proteinAlternativeName = proteinAlternativeName;
	}

	public String getGeneName() {
		return geneName;
	}

	public void setGeneName(String geneNameType) {
		this.geneName = geneNameType;
	}

	public String getOrganismName() {
		return organismName;
	}

	public void setOrganismName(String organismNameType) {
		this.organismName = organismNameType;
	}

	public void setIri(String iri) {
		this.iri = "http://www.uniprot.org/uniprot/"+iri;
	}

public JSONObject toJSON() {
		
		// create base JSON-LD
		JSONObject output = new JSONObject();
		
		// add type, fixed
		output.put("@type", "skos:Concept");
		
		// add an ID, which is the IRI of the term
		JSONArray iriWrapperArray = new JSONArray();
		iriWrapperArray.put(this.iri);
		output.put("skos:exactMatch", iriWrapperArray);
		
		// sub-object for storing the label + language info
		JSONObject labelObject = new JSONObject();
		JSONArray labelWrapperArray = new JSONArray();
		labelObject.put("@value", this.label);
		labelObject.put("@language", "eng");
		labelWrapperArray.put(labelObject);
		
		output.put("skos:prefLabel", labelWrapperArray);
		
		if(this.uniprotAccession != null && !this.uniprotAccession.isEmpty()) {
			output.put("uniprotAccession", new JSONArray(this.uniprotAccession));
		}
		
		if(this.uniprotName != null && !this.uniprotName.isEmpty()) {
			output.put("uniprotName", this.uniprotName);
		}

		if(this.proteinRecommendedName != null && !this.proteinRecommendedName.isEmpty()) {
			output.put("proteinRecommendedName", this.proteinRecommendedName);
		}
		
		if(this.proteinAlternativeName != null && !this.proteinAlternativeName.isEmpty()) {
			output.put("proteinAlternativeName", this.proteinAlternativeName);
		}
		
		if(this.geneName != null && !this.geneName.isEmpty()) {
			output.put("geneName", this.geneName);
		}
		
		if(this.organismName != null && !this.organismName.isEmpty()) {		// sub-object for storing the label + language info		
			if(this.synonym != null && !this.synonym.isEmpty()) {
				output.put("skos:altLabel", this.synonym);
			}
			output.put("organismName", this.organismName);
		}
		
		// sub-object for storing the label + language info		
		if(this.synonym != null && !this.synonym.isEmpty()) {
			
			JSONArray altLabelArray = null;
			
			try {
				//synonyms were stored as an array of objects
				altLabelArray = new JSONArray(this.synonym);
			} catch(JSONException je) {
				// synonym was stored as a single element, not an array
				altLabelArray = new JSONArray();
				altLabelArray.put(this.synonym);
			}
			
			
			
			output.put("skos:altLabel", altLabelArray);
		}
		
		if(this.ancestors != null && !this.ancestors.isEmpty()) {
			output.put("@hierarchicalAncestors", this.ancestors);
		}
		
		return output;
	}
	
}
