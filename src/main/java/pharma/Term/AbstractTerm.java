package pharma.Term;


import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@NodeEntity
/**
 * Class to describe the general structure of a term
 * Also the standard JSON return format is defined here 
 * @author asztrik
 *
 */
public abstract class AbstractTerm {
	
	/**
	 *  An Id is needed for the Persistence API
	 */
	@Id @GeneratedValue private Long id;

	/**
	 *  Here are the fields that all the Terms have
	 */
	@Index(unique=true) 
	protected String iri;
	
	protected String label;

	protected String synonym;
	
	protected String ancestors;
	
	protected String ontoclass;
	
	@Relationship(type = "CHILD", direction = Relationship.OUTGOING)
	protected List<AbstractTerm> hierarchy;

	private static final Logger logger = LoggerFactory.getLogger(AbstractTerm.class);
	
	/**
	 *  A shorter form of the suggest, only returns a minimal label + IRI object
	 *  used by /suggest to be followed by getData() calls
	 * @return
	 */
	public JSONObject toIriLabelJSON() {
		
		JSONObject output = new JSONObject();
		
		JSONArray labelArray = new JSONArray();
		JSONObject labelValueObj = new JSONObject();
		
		labelValueObj.put("@value", this.label);
		
		JSONObject labelLangObj = new JSONObject();
		
		labelLangObj.put("@language", "eng");
		
		labelArray.put(labelValueObj);
		labelArray.put(labelLangObj);
		
		
		output.put("skos:prefLabel", labelArray);
		
		// IRI, however always single must be in an array (req.)
		JSONArray iriArray = new JSONArray();
		
		iriArray.put(this.iri);
		
		output.put("skos:exactMatch", iriArray);
		
		return output;
	}
	
	
	/**
	 *  All the Terms need a method that converts them to a JSON
	 *  But the exact format / content depends on the FE fields...
	 *  you may redefine them at the individual Term Classes
	 * @return
	 */
	public JSONObject toJSON() {
		
		// create base JSON-LD
		JSONObject output = new JSONObject();
		
		// add type, fixed
		output.put("@type", "skos:Concept");
		
		// add an ID, which is the IRI of the term
		JSONArray iriWrapperArray = new JSONArray();
		iriWrapperArray.put(this.iri);
		output.put("skos:exactMatch", iriWrapperArray);
		
		// sub-object for storing the label + language info
		JSONObject labelObject = new JSONObject();
		JSONArray labelWrapperArray = new JSONArray();
		labelObject.put("@value", this.label);
		labelObject.put("@language", "eng");
		labelWrapperArray.put(labelObject);
		
		output.put("skos:prefLabel", labelWrapperArray);
		
		// sub-object for storing the label + language info		
		if(this.synonym != null && !this.synonym.isEmpty()) {
			
			JSONArray altLabelArray = null;
			
			try {
				//synonyms were stored as an array of objects
				altLabelArray = new JSONArray(this.synonym);
			} catch(JSONException je) {
				// synonym was stored as a single element, not an array
				altLabelArray = new JSONArray();
				altLabelArray.put(this.synonym);
			}
			
			ArrayList<String> synonymLabels = new ArrayList<String>();
			JSONArray duplicateFilteredSynonyms = new JSONArray();
			
			try {
				for(Object syn : altLabelArray) {
					JSONObject jsonSynObj = (JSONObject) syn;
					if(!synonymLabels.contains(jsonSynObj.get("@value").toString())) {
						synonymLabels.add(jsonSynObj.get("@value").toString());
						duplicateFilteredSynonyms.put(jsonSynObj);
					}
				} 
			}
			catch (JSONException jse) {
				// This is not a core feature, no action needed on fail
				//it is supposed to work 90%of the time
				logger.error("Duplicate filtering failed, continuing");
			}
			
			
			output.put("skos:altLabel", duplicateFilteredSynonyms);
		}

			// sub-object for storing the hierarchy info
		//	if(this.ancestors != null && !this.ancestors.isEmpty()) {
		//		output.put("rdfs:label", this.ancestors);
		//}
	if(this.ancestors != null && !this.ancestors.isEmpty()) {
JSONObject labelObject2 = new JSONObject();
		JSONArray labelWrapperArray2 = new JSONArray();
			labelObject2.put("@value", this.ancestors);
		labelObject2.put("@language", "eng");
		labelWrapperArray2.put(labelObject2);
		output.put("rdfs:label", labelWrapperArray2);
	}
		
		return output;
	}
	
	// getters and setters...
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIri() {
		return iri;
	}

	public void setIri(String iri) {
		this.iri = iri;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getSynonym() {
		return synonym;
	}

	public void setSynonym(String synonym) {
		this.synonym = synonym;
	}
	
	public String getAncestors() {
		return ancestors;
	}

	public void setAncestors(String ancestors) {
		this.ancestors = ancestors;
	}	
	
	public List<AbstractTerm> getParent() {
		return this.hierarchy;
	}

	public void setParent(List<AbstractTerm> parentlist) {
		this.hierarchy = parentlist;
	}

	
	public String getOntoClass() {
		return ontoclass;
	}

	public void setOntoClass(String ontoClass) {
		this.ontoclass = ontoClass;
	}
	
	
}


