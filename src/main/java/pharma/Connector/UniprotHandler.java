package pharma.Connector;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import pharma.Term.UniprotTerm;

/**
 * Parses and processes an XML term to a DB node
 * Required structure by the SAX package
 * @author asztrik
 *
 */
public class UniprotHandler extends DefaultHandler {

	/// Triggers
	boolean bEntry = false;
	boolean bAccession = false;
	boolean bProtein = false;
	boolean bRecommendedName = false;
	boolean bAlternativeName = false;
	boolean bFullName = false;
	boolean bGene = false;
	boolean bName = false;
	boolean bOrganism = false;
	
	private ArrayList<UniprotTerm> uniprotTermList = new ArrayList<UniprotTerm>();
	private UniprotTerm uniprotTerm = null;
	
	public ArrayList<UniprotTerm> getTerms() {
		return uniprotTermList;
	}
	
	/**
	 * Sets the triggers for the keywords: add here the exact ones
	 */
	@Override
	public void startElement(String uri, 
			   String localName, String qName, Attributes attributes) throws SAXException {
	
		if(qName.equalsIgnoreCase("entry")) {
			bEntry = true;
		} else if(qName.equalsIgnoreCase("accession")) {
			bAccession = true;
		} else if(qName.equalsIgnoreCase("protein")) {
			bProtein = true;
		} else if(qName.equalsIgnoreCase("recommendedName")) {
			bRecommendedName = true;
		} else if(qName.equalsIgnoreCase("alternativeName")) {
			bAlternativeName = true;
		} else if(qName.equalsIgnoreCase("fullName")) {
			bFullName = true;
		} else if(qName.equalsIgnoreCase("gene")) {
			bGene = true;
		} else if(qName.equalsIgnoreCase("name")) {
			bName = true;
		} else if(qName.equalsIgnoreCase("organism")) {
			bOrganism = true;
		}
	}

	/**
	 * Sets the triggers for the end of a term
	 */
	@Override
	public void endElement(
				String uri, String localName, String qName) throws SAXException {
		
		if(qName.equalsIgnoreCase("entry")) {
			bEntry = false;
		} else if(qName.equalsIgnoreCase("accession")) {
			bAccession = false;
		} else if(qName.equalsIgnoreCase("protein")) {
			bProtein = false;
		} else if(qName.equalsIgnoreCase("recommendedName")) {
			bRecommendedName = false;
		} else if(qName.equalsIgnoreCase("alternativeName")) {
			bAlternativeName = false;
		} else if(qName.equalsIgnoreCase("fullName")) {
			bFullName = false;
		} else if(qName.equalsIgnoreCase("gene")) {
			bGene = false;
		} else if(qName.equalsIgnoreCase("name")) {
			bName = false;
		} else if(qName.equalsIgnoreCase("organism")) {
			bOrganism = false;
		}
		 
	}
	
	/**
	 * Specifies what should happen on trigger
	 * SAX processes XML sequentially, we assume a well-formed XML.
	 */
	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		
		String pattern = "^[A-Z0-9]{1,5}_[A-Z0-9]{1,5}$";
		Pattern r = Pattern.compile(pattern);
		
		/// If we get to "entry", a new term begins, reset everything
		if(bEntry) {
			if(uniprotTerm != null) {
				uniprotTermList.add(uniprotTerm);			
			}
			uniprotTerm = new UniprotTerm();
			bEntry = false;
			bAccession = false;
			bProtein = false;
			bRecommendedName = false;
			bFullName = false;
			bAlternativeName = false;
			bGene = false;
			bName = false;
			bOrganism = false;
		}
		
		/// Next the fields of a term. We assume they are inside of an "entry"
		if(bAccession) {
			uniprotTerm.setIri(new String(ch, start, length));
			if(uniprotTerm.getUniprotAccession() == null) {
				uniprotTerm.setUniprotAccession(new JSONArray('[' + new String(ch, start, length) + ']').toString());
			} else {
				JSONArray otherAccessions = new JSONArray(uniprotTerm.getUniprotAccession());
				otherAccessions.put(new String(ch, start, length));
				uniprotTerm.setUniprotAccession(otherAccessions.toString());
			}
		}

		//top level name
		if(bName) {
			String nameStr = new String(ch, start, length);
			Matcher m = r.matcher(nameStr);
			if(m.find()) {
				uniprotTerm.setLabel(nameStr);
				uniprotTerm.setUniprotName(nameStr);
			}
		}
		
		if(bProtein && bRecommendedName && bFullName) {
			
			if(uniprotTerm.getProteinRecommendedName() == null)
				uniprotTerm.setProteinRecommendedName(new String(ch, start, length));
			else
				uniprotTerm.setProteinRecommendedName(uniprotTerm.getProteinRecommendedName() + " -- " + new String(ch, start, length));
		}
		
		if(bProtein && bAlternativeName && bFullName) {
			
			if(uniprotTerm.getProteinAlternativeName() == null)
				uniprotTerm.setProteinAlternativeName(new String(ch, start, length));
			else
				uniprotTerm.setProteinAlternativeName(uniprotTerm.getProteinAlternativeName() + " -- " + new String(ch, start, length));
		}
		
		if(bGene && bName) {
			
			String geneStr = new String(ch, start, length);
			
			if(!uniprotTerm.getLabel().equals(geneStr)) {
				
				if(uniprotTerm.getGeneName() == null)
					uniprotTerm.setGeneName(geneStr);
				else
					uniprotTerm.setGeneName(uniprotTerm.getGeneName() + " -- " + geneStr);				
			}

			
		}
		
		if(bOrganism && bName) {
			
			String organismStr = new String(ch, start, length);
			
			if(!uniprotTerm.getLabel().equals(organismStr)) {
			
				if(uniprotTerm.getOrganismName() == null)
					uniprotTerm.setOrganismName(organismStr);
				else
					uniprotTerm.setOrganismName(uniprotTerm.getOrganismName() + " -- " + organismStr);
			
			}
			
		}
		
	}
	    
	
	
}
