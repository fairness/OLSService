package pharma.Connector;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.neo4j.driver.v1.exceptions.ClientException;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;

import pharma.Exception.ExternalServiceConnectorException;
import pharma.Repository.CellosaurusRepository;
import pharma.Term.AbstractTerm;
import pharma.Term.CellosaurusTerm;

/**
 * Custom Cellosaurus connector
 * Downloads the webpage as is and parses HTML
 * As of 12.2019 there is no usable API.
 * @author asztrik
 *
 */
public class CellosaurusConnector implements ExternalServiceConnector {

	
	protected CellosaurusRepository CellosaurusRepo;
	
	private String UrlBase;
	
	private String searchUrlBase;
	
    private static final Logger logger = LoggerFactory.getLogger(CellosaurusConnector.class);
    
    protected static ArrayList<String> visitedTerms = new ArrayList<String>();
    
	protected Properties props;
    
    private String iri;
    
    private URL url;
    
    protected static HashMap<String, String> parentLinkList = new HashMap<String, String>();
	
	public CellosaurusConnector() {	}
	
	public CellosaurusConnector(Properties props) {
		this.props = props;
		this.UrlBase = props.getProperty("cellosaurus.base");
		this.searchUrlBase = props.getProperty("cellosaurus.search");
	}
	
	public CellosaurusConnector(String iri, CellosaurusRepository eor) throws ExternalServiceConnectorException {
		this.CellosaurusRepo = eor;
		this.iri = iri;
		
		try {
			this.url = new URL(
					UrlBase+this.iri);
		} catch (MalformedURLException e1) {
			logger.error(e1.getMessage(), e1);
			throw new ExternalServiceConnectorException(e1.getMessage());
		}
	}
	
	

	/**
	 * This is the queryAndStoreOls that we have at other ontologies, the exact copy
	 * It is only moved to an other function because the entry function in this class
	 * for the update has to get the IRI-s that match a category.
	 * @param ontoClass
	 * @return
	 * @throws ExternalServiceConnectorException
	 */
	private HashMap<String, String> queryAndStoreOLSbyCategoryList(String ontoClass) throws ExternalServiceConnectorException {
		Document doc = null;

		boolean success = false;
		int attempts = 0;
		
		while(attempts < 3) {
			try {
				doc = Jsoup.connect(url.toString()).get();
				success = true;
				break;
			} catch (IOException e) {
				// we handle this after the last attempt
			}
			attempts++;
		}
		

		if(success) {
			String title = doc.title();

			CellosaurusTerm term = new CellosaurusTerm();
			
			term.setIri(url.toString());
			term.setLabel(title);
			term.setOntoClass(ontoClass);
			
			
		
			
		    Elements inputElements = doc.getElementsByTag("th");  
		    for (Element inputElement : inputElements) {   
		    	if(inputElement.text().equals("Hierarchy")) {	
		    		if(inputElement.nextElementSibling().text().substring(0, 8).equals("Children")) {
		    		// Cell begins with "Children": save all the href-targets as list of children
		    			for (Element childLink : inputElement.nextElementSibling().getElementsByTag("a")) {
		    				parentLinkList.putAll(processChildTerm(childLink.text(), parentLinkList, ontoClass));
		    			}    	            
		    		} else if(inputElement.nextElementSibling().text().substring(0, 6).equals("Parent")) {
		    		  // There is always only 1 parent in Cellosaurus
		    			Element parentLink = inputElement.nextElementSibling().getElementsByTag("a").get(0);
		    			parentLinkList.put(iri, parentLink.text());	    				
		    			int index = 0;	
		    		    // After that there might be some children too:
						for (Element childLink : inputElement.nextElementSibling().getElementsByTag("a")) {
							// Omit the first element, since it must be a parent we just processed
							if(index > 0) {
								parentLinkList.putAll(processChildTerm(childLink.text(), parentLinkList, ontoClass));
							}
							index++;
						}
		    		}
		    		
		    	} else if(inputElement.text().equals("Accession")) {
		    		term.setAccessionNumber(inputElement.nextElementSibling().text());
		    	} else if(inputElement.text().equals("Cell line name")) {
		    		term.setCellLineName(inputElement.nextElementSibling().text());
		    	} else if(inputElement.text().equals("Synonyms")) {
		    		term.setSynonym(inputElement.nextElementSibling().text());
		    	}
		    } 
				
			try {
				CellosaurusRepo.save(term);
			} catch (DataIntegrityViolationException e) {
				logger.debug(term.getIri() + " - duplicate IRI, not saved.");
				// is okay
			} catch (ClientException ce) {
				if(ce.getMessage().contains("already exists with label")) {
					logger.debug(term.getIri() + " - duplicate IRI, not saved.");
					// is okay
				} else {
					throw new ExternalServiceConnectorException("Client error: " + ce.getMessage() + " Could not connect to Cellosaurus, terminating update.");
				}
				
				
			}
		    
		    
			return parentLinkList;
		} else {
			throw new ExternalServiceConnectorException("Could not connect to Cellosaurus, terminating update.");
		}
	}
		
	@Override
	/**
	 * The flow here differs from the others. Here is a step injected: first we query 
	 * the IRI-list that matches a category, and then go thru this list as if they were
	 * all root terms in the app.props. everything else is as usual-
	 */
	public HashMap<String, String> queryAndStoreOLS(String ontoClass) throws ExternalServiceConnectorException {

		Document doc = null;
		
		ArrayList<String> irisToGet = new ArrayList<String>();
		
		String queryUrl = this.searchUrlBase + ontoClass;
		
		logger.info(queryUrl);
		
		boolean success = false;
		int attempts = 0;
		
		while(attempts < 3) {
			try {
				doc = Jsoup.connect(queryUrl.toString()).get();
				success = true;
				break;
			} catch (IOException e) {
				// we handle this after the last attempt
			}
			attempts++;
		}
		
		if(success) {
			Elements inputElements = doc.getElementsByTag("td");  
			for (Element inputElement : inputElements) {
				Elements aTag = inputElement.children();
				for(Element tag : aTag) {
					if(tag.hasText()) {
						irisToGet.add(inputElement.text());
					}
				}					
			}
		}
		
		logger.info(irisToGet.size()+"DONE");
		
		for(String iri: irisToGet) {
			logger.info(iri);
			this.setIri(iri);
			parentLinkList.putAll(queryAndStoreOLSbyCategoryList(ontoClass));
		}
		
		return parentLinkList;
	}

	
	private HashMap<String, String> processChildTerm(String childLinkText, HashMap<String, String> parentLinkList, String ontoClass) throws ExternalServiceConnectorException {

		if(!visitedTerms.contains(childLinkText)) {
			visitedTerms.add(childLinkText);
			// go recursive

			this.setIri(childLinkText);
			
			// add iri to the parent linking map
			parentLinkList.putAll(this.queryAndStoreOLSbyCategoryList(ontoClass));
			
		}
		
		return parentLinkList;
	}
	
	@Override
	public AbstractTerm retrieveAsJSON(String iri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void linkParents(String parentIri, String childIri) {

		List<AbstractTerm> parent = this.CellosaurusRepo.findByIri(parentIri);
		if(!parent.isEmpty()) {
			List<AbstractTerm> child = this.CellosaurusRepo.findByIri(childIri);
			if(!child.isEmpty()) {
				
				AbstractTerm parentToAdd = parent.get(0);
				AbstractTerm childTermToUpdate = child.get(0);
				
				List<AbstractTerm> currentParents = childTermToUpdate.getParent();
				
				if(currentParents == null || currentParents.isEmpty()) {
					currentParents = new ArrayList<AbstractTerm>();
				}
				
				currentParents.add(parentToAdd);
				childTermToUpdate.setParent(currentParents);
				CellosaurusRepo.save(childTermToUpdate);
				
			}
		}
		
		return;
	}

	@Override
	public void setIri(String iri) throws ExternalServiceConnectorException {
		this.iri = iri;
		try {
			this.url = new URL(
					UrlBase+this.iri);
		} catch (MalformedURLException e1) {
			logger.error(e1.getMessage(), e1);
			throw new ExternalServiceConnectorException(e1.getMessage());
		}
	}

	@Override
	public String getIri() {
		return iri;
	}

	@Override
	public void setRepo(Object repo) {
		this.CellosaurusRepo = (CellosaurusRepository)repo;

	}

	@Override
	public Object getRepo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveOne(String iri, String ontoclass) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public AbstractTerm saveOneTerm(String ontoClass, AbstractTerm pt) throws ExternalServiceConnectorException {
		// TODO Auto-generated method stub
		return null;
	}

}
