package pharma.Connector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pharma.Exception.ExternalServiceConnectorException;
import pharma.Term.AbstractTerm;

/**
 * Several ontologies are fetched via the EBI OLS
 * The ontologies may have different fields and requirements
 * But the core methods to call the external OLS and structures
 * are the same, the common metods are in this class
 * @author asztrik
 *
 */
public abstract class AbstractOlsConnector implements ExternalServiceConnector {

	protected URL url = null;
	
	protected HttpURLConnection conn = null;
	
	protected String iri;
	
	protected Properties props;
	
	protected static ArrayList<String> visitedTerms = new ArrayList<String>();
	
    private static final Logger logger = LoggerFactory.getLogger(AbstractOlsConnector.class);
	    
    /**
     * Save only one term, taking the URL set for the caller class
     * and not fetching the children. Used to save the Class parent (topmost) term.
     * @throws ExternalServiceConnectorException 
     */
    public AbstractTerm saveOneTerm(String ontoClass, AbstractTerm pt) throws ExternalServiceConnectorException {
		
    	try {
			conn = (HttpURLConnection) this.url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new ExternalServiceConnectorException(e.getMessage());
		}    	
		
		JSONObject json = null;
		
		try {
			if (conn.getResponseCode() != 200) {
			    throw new RuntimeException("Failed : HTTP error code : "
			        + conn.getResponseCode() + " URL: " + this.url);
			    }		
	        StringBuilder sb = new StringBuilder();        
	        String line;       
	        BufferedReader br = new BufferedReader(new InputStreamReader(
	                (this.conn.getInputStream())));
	        while ((line = br.readLine()) != null) {
	            sb.append(line);
	        }        
	        
	        if(sb.toString().isEmpty())
	        	throw new ExternalServiceConnectorException("Empty response from " + conn.getURL());	
	        	
	        json = new JSONObject(sb.toString());
	        
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new ExternalServiceConnectorException(e.getMessage());
		}
		
		
		try {
			pt.setIri(json.getString("iri"));
			pt.setLabel(json.getString("label"));
			pt.setSynonym(json.getString("label"));
			pt.setOntoClass(ontoClass);
		} catch (JSONException je) {
			logger.error(je.getMessage(), je);
			throw new ExternalServiceConnectorException(je.getMessage());
		}
		
		logger.debug("SOT get "+pt.getIri());
		
		return pt;
    }
    
    
	/**
	 * Connects to the url set for this class and retrieves all the terms to one IRI as JSONArray
	 * A null customUrl parameter means taking the classes URL property
	 * Otherwise the supplied one - is used for getting hierarchicalAncestry
	 * @return
	 * @throws ExternalServiceConnectorException
	 */
	public JSONArray connectAndGetJSON(ConnectionPurpose cp, URL customUrl) throws ExternalServiceConnectorException {
		
		URL url2Connect;
		
		if(customUrl == null) {
			url2Connect = this.url;
		} else {
			url2Connect = customUrl;
		}
		
		try {
			conn = (HttpURLConnection) url2Connect.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new ExternalServiceConnectorException(e.getMessage());
		}
		
		JSONObject json = null;
		
		try {
			if (conn.getResponseCode() != 200) {
			    throw new RuntimeException("Failed : HTTP error code : "
			        + conn.getResponseCode());
			    }		
	        StringBuilder sb = new StringBuilder();        
	        String line;       
	        BufferedReader br = new BufferedReader(new InputStreamReader(
	                (this.conn.getInputStream())));
	        while ((line = br.readLine()) != null) {
	            sb.append(line);
	        }        
	        
	        if(sb.toString().isEmpty())
	        	throw new ExternalServiceConnectorException("Empty response from " + conn.getURL());	
	        	
	        json = new JSONObject(sb.toString());
	        
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new ExternalServiceConnectorException(e.getMessage());
		}
		
		JSONArray result = new JSONArray();
		
		try {
			
			if(cp.equals(ConnectionPurpose.TERMS)) {
				// on success: there are multiple children
				result = json.getJSONObject("_embedded").getJSONArray("terms");
			} else if(cp.equals(ConnectionPurpose.PAGES)) {
				result = new JSONArray();
				result.put(json.getJSONObject("page").get("totalElements"));
			}
			
					
		} catch (JSONException je) {
			if(je.getMessage().contains("[\"_embedded\"] not found")) {
				// on fail: no children recursion should end.
				// no need to log full stack trace, it is an okay case
				result = null;
			} else {
				logger.error(je.getMessage());
				throw new ExternalServiceConnectorException("Error parsing JSON from external OLS. URL: " + url2Connect);
			}
			
			
		}
		
    	return result;
	}
	
	/**
	 * Returns the queried term to be processed in the ontology-specific class
	 * @param terms
	 * @param i
	 * @param ontoClass
	 * @param pt
	 * @return
	 * @throws ExternalServiceConnectorException
	 */
	public AbstractTerm retrieveTerm(JSONArray terms, int i, String ontoClass, AbstractTerm pt) throws ExternalServiceConnectorException {
		JSONObject term = terms.getJSONObject(i);
		
		// Create Entity that will be persisted
		pt.setIri(term.getString("iri"));
				
		String labelString = "\"" + term.getString("label");
		String ancestorsString = new String();
		JSONArray synonymsArray = new JSONArray();
		
		pt.setLabel(term.getString("label"));
		
		try {
			JSONArray synonymsObj = term.getJSONArray("synonyms");
    		
    		for(int j=0; j<synonymsObj.length(); j++) {
    			JSONObject synonym = new JSONObject();
    			synonym.put("@value", synonymsObj.get(j));
    			synonym.put("@language", "@eng");
    			synonymsArray.put(synonym);
    		}
    		
    		
    		JSONObject linksObj = term.getJSONObject("_links");
    		JSONObject hierarchAncestObj = linksObj.getJSONObject("hierarchicalAncestors");
    		
    		JSONArray ancestorArray = connectAndGetJSON(
    				ConnectionPurpose.TERMS, 
    				new URL(hierarchAncestObj.getString("href"))
    				);
    		
    		if(ancestorArray != null && ancestorArray.length() > 0) {
	    		for(int j=0; j<ancestorArray.length(); j++) {
	    			JSONObject ancestor = (JSONObject) ancestorArray.get(j);
	    			ancestorsString = ancestorsString + "--" + ancestor.getString("label");
	    		}
    		}
    		
    		
		} catch (JSONException jse) {
			if(jse.getMessage().contains("[\"synonyms\"] is not a JSONArray")) {
				// Do nothing, it's normal...
				// it means that the term had empty or no synonyms array
			} else {
				logger.error(jse.getMessage() + pt.getIri());
				throw new ExternalServiceConnectorException(jse.getMessage());
			}
		} catch (MalformedURLException e) {
			logger.error(e.getMessage(), e);
			throw new ExternalServiceConnectorException(e.getMessage());
		} catch (ExternalServiceConnectorException e) {
			logger.error(e.getMessage(), e);
			throw new ExternalServiceConnectorException(e.getMessage());
		}
		
		labelString = labelString + "\"";
		
		pt.setAncestors(ancestorsString);
		pt.setSynonym(synonymsArray.toString());
		pt.setOntoClass(ontoClass);
		
		logger.debug("Retrieved: " + pt.getIri());
		
		return pt;
	

	}
		

	/**
	 * Helper function to fix the paging problem of the OLS
	 * The OLS returns by default only a 20 long page of the child-list
	 * This excludes some elements of the result. Appending the ?size=N& to
	 * the requests makes all the terms available on one page.
	 * 
	 * Why so and why not handle paging?
	 * 
	 * A JSON of 20 records is 16 kB, approx. 2000 children would result 
	 * in a ~ 2 MB response, which is still OK to handle. Note: this is
	 * only a part of the update method, which is only supposed to run 
	 * weekly to fill the DB and not to serve user requests.
	 * @param originalUrl
	 * @return
	 * @throws ExternalServiceConnectorException 
	 */
	public String appendPageToBaseurl(String originalUrl) throws ExternalServiceConnectorException {
		
		int totalElementNum = 0;
		
		// first load the page
		try {
			this.url = new URL(originalUrl);
		} catch (MalformedURLException e) {
			logger.error(e.getMessage(), e);
			throw new ExternalServiceConnectorException(e.getMessage());
		}		
		
		//extract the "pages" Object & get total size
		try {
			totalElementNum = (int) connectAndGetJSON(ConnectionPurpose.PAGES, null).get(0);
		} catch (ExternalServiceConnectorException e) {
			logger.error(e.getMessage(), e);
			throw new ExternalServiceConnectorException(e.getMessage());
		}		
		
		// append size parameter
		String newUrl = null;
		
		
		if(originalUrl.endsWith("/children")) {
			newUrl = originalUrl.replace("/children", "/children?size=" + String.valueOf(totalElementNum));
		}
		else {
			newUrl = originalUrl.replace("?id", "?size=" + String.valueOf(totalElementNum) + "&id");	
		}
		
		return newUrl;
	}
	
	@Override
	public AbstractTerm retrieveAsJSON(String iri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void linkParents(String url, String childIri) throws ExternalServiceConnectorException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setIri(String iri) throws ExternalServiceConnectorException {
		// TODO Auto-generated method stub

	}

	@Override
	public String getIri() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRepo(Object repo) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object getRepo() {
		// TODO Auto-generated method stub
		return null;
	}

}
