package pharma.Connector;

import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.xml.sax.InputSource;


import pharma.Exception.ExternalServiceConnectorException;
import pharma.Repository.UniprotRepository;
import pharma.Term.AbstractTerm;
import pharma.Term.UniprotTerm;

/**
 * Custom, XML based uniprot connector
 * Uniprot API could return all the 600k terms in a single XML
 * There is an XML stream + paging approach applied for stability
 * @author asztrik
 *
 */
public class UniprotConnector implements ExternalServiceConnector {

	
	protected UniprotRepository UniProtRepo;
	
	protected Properties props;
	
	private String xmlUrlBase;
	
    private static final Logger logger = LoggerFactory.getLogger(UniprotConnector.class);
    
    private int limit;
	
    private int offset;
    
    /**
     *  if the query broke down for some reason, you do not need to begin it from the first node,
     *  but from the one you specify in application.properties
     */
    private int startingOffset;
    
    /**
     *  number of the terms in SwissProt. Bug #20549 showed that the query can exceed the limit and go further
     *  this is meant to stop that.
     */

    private int termcount;
    
    private boolean lastTermReached = false;
    
	public UniprotConnector() {	}
	
	public UniprotConnector(Properties props) {
		this.props = props;
		this.xmlUrlBase = props.getProperty("uniprot.base");
		this.limit = Integer.valueOf(props.getProperty("uniprot.limit"));
		this.termcount = Integer.valueOf(props.getProperty("uniprot.termcount"));
		this.startingOffset = Integer.valueOf(props.getProperty("uniprot.startingOffset"));
		
		this.offset = this.startingOffset;
	}
	
	@Override
	public HashMap<String, String> queryAndStoreOLS(String ontoClass) throws ExternalServiceConnectorException {

		while(!this.lastTermReached) {
			try {

		    	SAXParserFactory spf = SAXParserFactory.newInstance();
		    	SAXParser sp = spf.newSAXParser();
		    	URL linkURL = new URL(xmlUrlBase+"&limit="+this.limit+"&offset="+this.offset);
		    	UniprotHandler uniprotHandler = new UniprotHandler();
		    	
		    	// Parse the whole XML into a List of terms
		    	sp.parse(new InputSource(linkURL.openStream()), uniprotHandler);

		    	
		    	// traverse the term list and save them. This is separated from the parsing
		    	// for the case that some other method was needed here (like linkParents)
		    	for (Iterator<UniprotTerm> i = uniprotHandler.getTerms().iterator(); i.hasNext();) {
		    	    UniprotTerm term = i.next();
		    	    term.setOntoClass(ontoClass);
		    	    /// if there is no synonym, set the field to the label so we do not get an invalid JSON
		    	    if(term.getSynonym() == null || term.getSynonym().isEmpty())
		    	    	term.setSynonym(term.getLabel());
		    		try {
			    		this.UniProtRepo.save(term);
		    		} catch (DataIntegrityViolationException e) {
		    			logger.debug(term.getIri() + " - duplicate IRI, not saved.");
		    			// is okay
		    		} catch (Exception ex) {
		    			if(ex.getMessage().contains("already exists with label")) {
				    		// do nothing, skip term
				    	} else {
					    	logger.error(ex.getMessage(), ex);
					    	// it is most likely a parsing exception from SAX, can be passed upwards
					    	throw new ExternalServiceConnectorException(ex.getMessage());	
				    	}
		    		}
		    		logger.debug(term.getIri() + " saved.");
		    	}
			
		    } catch (Exception e) {
		    	
		    	if(e.getMessage().contains("Premature end of file")) {
		    		// we got an empty XML which means we reached the end
		    		//return as normal
		    		this.lastTermReached = true;
		    	} else {
			    	logger.error(e.getMessage(), e);
			    	// it is most likely a parsing exception from SAX, can be passed upwards
			    	throw new ExternalServiceConnectorException(e.getMessage());	
		    	}
		    	
		    }	
			
			if(this.offset < this.termcount) {
				this.offset = this.offset + this.limit;
				logger.info("Getting the next "+this.limit+" terms, beginning from "+this.offset);
			}
			else {
				logger.info("Max. term count of " +this.termcount + " reached, stopping.");
			    // quit the hard way.
				return new HashMap<String, String>();
			}
			
		}
		
	    // need to return this type
		return new HashMap<String, String>();
	}

	@Override
	public AbstractTerm retrieveAsJSON(String iri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void linkParents(String url, String childIri) throws ExternalServiceConnectorException {
		// No parent linking for UniProt necessary

	}

	@Override
	public void setIri(String iri) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getIri() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRepo(Object repo) {
		this.UniProtRepo = (UniprotRepository)repo;
	}

	@Override
	public Object getRepo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveOne(String iri, String ontoclass) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public AbstractTerm saveOneTerm(String ontoClass, AbstractTerm pt) throws ExternalServiceConnectorException {
		// TODO Auto-generated method stub
		return null;
	}

}
