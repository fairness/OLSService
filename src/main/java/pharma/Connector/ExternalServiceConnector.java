package pharma.Connector;

import java.util.HashMap;
import pharma.Exception.ExternalServiceConnectorException;
import pharma.Term.AbstractTerm;

/**
 * Interface for connectors: purpose is to connect to an external source
 * and save terms into the DB directly
 * after all the terms are retrieved, add the CHILD relations too
 * (reason for this: multiple parents are allowed)
 * @author asztrik
 *
 */
public interface ExternalServiceConnector {
	
	/**
	 *  Calls a given OLS and parses term(s)
	 * @param ontoClass
	 * @return
	 * @throws ExternalServiceConnectorException
	 */
	public HashMap<String, String> queryAndStoreOLS(String ontoClass) throws ExternalServiceConnectorException;
	
	/**
	 *  Retrieves a previously persisted term as a JSON Object
	 * @param iri
	 * @return
	 */
	public AbstractTerm retrieveAsJSON(String iri);
	
	/**
	 *  Links the parents to the children
	 * @param url
	 * @param childIri
	 * @throws ExternalServiceConnectorException
	 */
	public void linkParents(String url, String childIri) throws ExternalServiceConnectorException;
	
	public void setIri(String iri) throws ExternalServiceConnectorException;
	
	public String getIri();
	
	public void setRepo(Object repo);
	
	public Object getRepo();
	
	public void saveOne(String iri, String ontoclass) throws ExternalServiceConnectorException;
	
	public AbstractTerm saveOneTerm(String ontoClass, AbstractTerm pt) throws ExternalServiceConnectorException;

}
