package pharma.Connector;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONObject;
import org.neo4j.driver.v1.exceptions.ClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pharma.Exception.ExternalServiceConnectorException;
import pharma.Repository.MondoRepository;
import pharma.Term.AbstractTerm;
import pharma.Term.MondoTerm;

/**
 * EBI OLS Based Mondo connector
 * @author asztrik
 *
 */
public class MondoConnector extends AbstractOlsConnector {

	protected MondoRepository MondoRepo;
	
	protected String baseUrl;
	
	protected String baseUrlEncoded;
	
    private static final Logger logger = LoggerFactory.getLogger(MondoConnector.class);
	
	public MondoConnector() {	}	
	
	public MondoConnector(Properties props) {
		this.props = props;
		this.baseUrl = props.getProperty("mondo.base");
		this.baseUrlEncoded = props.getProperty("mondo.encoded");
	}
	
	public MondoConnector(URL url, HttpURLConnection conn, String iri, MondoRepository eor) {
		this.iri = iri;
		this.url = url;
		this.conn = conn;
		this.MondoRepo = eor;
	}
	
	public MondoConnector(String iri, MondoRepository eor) throws ExternalServiceConnectorException {
		
		this.MondoRepo = eor;
		this.iri = iri;
		
		// This adds a &size parameter to the URL so that it returns
		// all the terms, without paging
		this.baseUrl = appendPageToBaseurl(this.baseUrl);
		
		
		try {
			this.url = new URL(
					baseUrl+this.iri);
		} catch (MalformedURLException e1) {
			logger.error(e1.getMessage(), e1);
			throw new ExternalServiceConnectorException(e1.getMessage());
		}
		
		
		try {
			this.conn = (HttpURLConnection) this.url.openConnection();
			this.conn.setRequestMethod("GET");
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new ExternalServiceConnectorException(e.getMessage());
		}
		
		this.conn.setRequestProperty("Accept", "application/json");
	}
	

	/**
	 * Gets only the parents of a term as a list
	 * @param url
	 * @return
	 * @throws ExternalServiceConnectorException
	 */
	public void linkParents(String url, String childIri) throws ExternalServiceConnectorException {
		
		try {
			this.url = new URL(url);
		} catch (MalformedURLException e) {
			logger.error(e.getMessage(), e);
			throw new ExternalServiceConnectorException(e.getMessage());
		}
		
		JSONArray terms = connectAndGetJSON(ConnectionPurpose.TERMS, null);
		  	
    	// get the terms one by one
    	for (int i=0; i < terms.length(); i++) {
    		JSONObject term = terms.getJSONObject(i);
    		
    		// At this moment there are no parents to be found, need to do the linking later!!!
    		List<AbstractTerm> child = this.MondoRepo.findByIri(childIri);
    		if(!child.isEmpty()) {
    			List<AbstractTerm> parent = this.MondoRepo.findByIri(term.getString("iri"));
    			if(!parent.isEmpty()) {
    				AbstractTerm parentToAdd = parent.get(0);
    				AbstractTerm childTermToUpdate = child.get(0);
    				
    				List<AbstractTerm> currentParents = childTermToUpdate.getParent();
    				
    				if(currentParents == null || currentParents.isEmpty()) {
    					currentParents = new ArrayList<AbstractTerm>();
    				}
    				
    				currentParents.add(parentToAdd);
					childTermToUpdate.setParent(currentParents);
					MondoRepo.save(childTermToUpdate);
    			}
    		}
    	}
	
	}

	/**
	 * Connects to the URL set and gets the terms as JSON, also sets the parents
	 * Returns a dictionary with the links to the parents of the terms.
	 * How the Dict looks like:
	 * [TermIri] --> [ParentLink]
	 * Both unique.
	 */
	@Override
	public HashMap<String, String> queryAndStoreOLS(String ontoClass) throws ExternalServiceConnectorException {
		
		if(this.iri.isEmpty() || this.iri == null) {
				throw new ExternalServiceConnectorException("Iri is not set, the OLS would give 404");	
		}
	
		// All the terms for one query as array
    	JSONArray terms = connectAndGetJSON(ConnectionPurpose.TERMS, null);
    	
    	HashMap<String, String> parentLinkList = new HashMap<String, String>();
    	
		if(terms == null)
			return parentLinkList; // return empty list if there are no more children.
   	
    	// get the terms one by one
    	for (int i=0; i < terms.length(); i++) {

    		MondoTerm term = new MondoTerm();
    		
    		term = (MondoTerm)retrieveTerm(terms, i,  ontoClass, term);
    		
    		parentLinkList.put( term.getIri(), terms.getJSONObject(i).getJSONObject("_links").getJSONObject("parents").getString("href"));
    		
    		try {
    			MondoRepo.save(term);
    		} catch (ClientException e) {
    			if(e.getMessage().contains("already exists")) {
    				logger.debug("Skipping duplicate node");
    				// is okay
    			} else {
    				logger.error(e.getMessage(), e);
    				throw new ExternalServiceConnectorException(e.getMessage());
    			}
    		}  	
    		
    		
    		// Now get the iri
    		String iri = term.getIri();
    		
    		// Format for the next request
    		iri.replace(this.props.getProperty("purloboprefix"), "");
    		iri.replace("_", ":");
    		
    		if(!visitedTerms.contains(iri)) {
    			visitedTerms.add(iri);
    			// go recursive
    			this.setIri(iri);
    			parentLinkList.putAll(this.queryAndStoreOLS(ontoClass));
    		}		
    		
    	}
    	
    	return parentLinkList;
    
	}
	
	@Override
	public AbstractTerm retrieveAsJSON(String iri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRepo(Object repo) {
		this.MondoRepo = (MondoRepository)repo;
		
	}

	@Override
	public Object getRepo() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getIri() {
		return iri;
	}

	public void setIri(String iri) throws ExternalServiceConnectorException {
		this.iri = iri;
		// This adds a &size parameter to the URL so that it returns
		// all the terms, without paging
		String combinedUrl = appendPageToBaseurl(this.baseUrl+this.iri);

		try {
			this.url = new URL(combinedUrl);
		} catch (MalformedURLException e1) {
			logger.error(e1.getMessage(), e1);
			throw new ExternalServiceConnectorException(e1.getMessage());
		}
	}
		
	/**
	 * fetches and saves one term
	 * @throws ExternalServiceConnectorException 
	 */
	public void saveOne(String iri, String ontoclass) throws ExternalServiceConnectorException {
		this.iri = iri;
		try {
			this.url = new URL(
					this.baseUrlEncoded+URLEncoder.encode(URLEncoder.encode(this.iri, "UTF-8"), "UTF-8"));
		} catch (MalformedURLException e1) {
			logger.error(e1.getMessage(), e1);
			throw new ExternalServiceConnectorException(e1.getMessage());
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage(), e);
			throw new ExternalServiceConnectorException(e.getMessage());
		}		
		MondoTerm term = new MondoTerm();
		try {
			term = (MondoTerm)saveOneTerm(ontoclass, term);
			MondoRepo.save(term);
		} catch (ClientException e) {
			if(e.getMessage().contains("already exists")) {
				logger.debug("Skipping duplicate node");
				// is okay
			} else {
				logger.error(e.getMessage(), e);
				throw new ExternalServiceConnectorException(e.getMessage());
			}
		} 
	}	

}
