package pharma.Repository;

/**
 * This repository is an instance of the AbstractRepo with all the common queries
 * it can be used to fetch terms independently from ontologies
 * - used for getData(iri)
 * - might be used to get mixed ontology queries...
 * @author asztrik
 *
 */
public interface GeneralRepository extends AbstractRepository {

}
