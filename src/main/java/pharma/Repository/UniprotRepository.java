package pharma.Repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.query.Param;

import pharma.Term.AbstractTerm;

/**
 * Custom Uniprot repo methods
 * Note: search is extended to many fileds!
 * @author asztrik
 *
 */
public interface UniprotRepository extends AbstractRepository {
	/**
	 * Retrieves a term by synonym , searching only the synonym field
	 * @param synonym
	 * @return
	 */
	@Query("MATCH (t:UniprotTerm) WHERE (lower(t.synonym) CONTAINS lower({synonym}) OR  lower(t.label) CONTAINS lower({synonym}) OR lower(t.uniprotName) CONTAINS lower({synonym}) OR lower(t.proteinRecommendedName) CONTAINS lower({synonym}) OR lower(t.uniprotAccession) CONTAINS lower({synonym}) OR lower(t.geneName) CONTAINS lower({synonym}) OR lower(t.organismName) CONTAINS lower({synonym}) OR lower(t.proteinAlternativeName) CONTAINS lower({synonym})) RETURN t")
	List<AbstractTerm> findBySynonym(@Param("synonym") String synonym);
	
	/**
	 * Retrieves a term by synonym and ontology class
	 * @param synonym
	 * @param className
	 * @return
	 */
	@Query("MATCH (t:UniprotTerm) WHERE (lower(t.synonym) CONTAINS lower({synonym}) OR lower(t.label) CONTAINS lower({synonym}) OR lower(t.uniprotName) CONTAINS lower({synonym}) OR lower(t.proteinRecommendedName) CONTAINS lower({synonym}) OR lower(t.uniprotAccession) CONTAINS lower({synonym}) OR lower(t.geneName) CONTAINS lower({synonym}) OR lower(t.organismName) CONTAINS lower({synonym}) OR lower(t.proteinAlternativeName) CONTAINS lower({synonym})) AND t.ontoclass CONTAINS {className} RETURN t")
	List<AbstractTerm> findBySynonym(@Param("synonym") String synonym, @Param("className") String className);
}
