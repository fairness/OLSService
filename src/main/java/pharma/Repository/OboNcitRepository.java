package pharma.Repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.query.Param;

import pharma.Term.AbstractTerm;


/**
 * Custom OboNcit repo methods
 * @author asztrik
 *
 */
public interface OboNcitRepository extends AbstractRepository {
	/**
	 * Retrieves a term by synonym , searching only the synonym field
	 * @param synonym
	 * @return
	 */
	@Query("MATCH (t:OboNcitTerm) WHERE lower(t.synonym) CONTAINS lower({synonym}) OR lower(t.label) CONTAINS lower({synonym}) RETURN t")
	List<AbstractTerm> findBySynonym(@Param("synonym") String synonym);
	
	/**
	 * Retrieves a term by synonym and ontology class
	 * @param synonym
	 * @param className
	 * @return
	 */
	@Query("MATCH (t:OboNcitTerm) WHERE ( lower(t.synonym) CONTAINS lower({synonym}) OR lower(t.label) CONTAINS lower({synonym}) ) AND t.ontoclass CONTAINS {className} RETURN t")
	List<AbstractTerm> findBySynonym(@Param("synonym") String synonym, @Param("className") String className);
}
