package pharma.Exception;

/**
 * General exception to handle external OLS faults
 * TODO
 * --- 
 * @author asztrik
 *
 */
public class ExternalServiceConnectorException extends Exception {

	private static final long serialVersionUID = 1L;

	public ExternalServiceConnectorException(String text) {
		super(text);
	}
	
}
