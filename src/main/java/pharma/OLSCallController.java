	package pharma;


import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pharma.Connector.BaoConnector;
import pharma.Connector.CellosaurusConnector;
import pharma.Connector.ChebiConnector;
import pharma.Connector.GoConnector;
import pharma.Connector.ExternalServiceConnector;
import pharma.Connector.FaoConnector;
import pharma.Connector.CloConnector;
import pharma.Connector.MondoConnector;
import pharma.Connector.NcbiTaxonConnector;
import pharma.Connector.OboNcitConnector;
import pharma.Connector.OmitConnector;
import pharma.Connector.PlantConnector;
import pharma.Connector.UberonConnector;
import pharma.Connector.UniprotConnector;
import pharma.Exception.ExternalServiceConnectorException;
import pharma.Repository.AbstractRepository;
import pharma.Repository.BaoRepository;
import pharma.Repository.CellosaurusRepository;
import pharma.Repository.ChebiRepository;
import pharma.Repository.GoRepository;
import pharma.Repository.FaoRepository;
import pharma.Repository.CloRepository;
import pharma.Repository.GeneralRepository;
import pharma.Repository.MondoRepository;
import pharma.Repository.NcbiTaxonRepository;
import pharma.Repository.OboNcitRepository;
import pharma.Repository.OmitRepository;
import pharma.Repository.PlantRepository;
import pharma.Repository.UberonRepository;
import pharma.Repository.UniprotRepository;
import pharma.Term.AbstractTerm;

import org.json.JSONArray;
import org.json.JSONObject;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
//Import log4j classes.
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;


@CrossOrigin(maxAge = 3600)
@RestController
/**
 * Class handling the method requests
 * Actual "entry ponit"
 * @author asztrik
 *
 */
public class OLSCallController {

	@Autowired
	private GoRepository goRepo;
	
	@Autowired
	private OboNcitRepository oboNcitRepo;
	
	@Autowired
	private ChebiRepository chebiRepo;
	
	@Autowired
	private NcbiTaxonRepository ncbiTaxonRepo;
	
	@Autowired
	private MondoRepository mondoRepo;

	@Autowired
	private FaoRepository faoRepo;

	@Autowired
	private CloRepository cloRepo;
	
	@Autowired
	private UberonRepository uberonRepo;
	
	@Autowired
	private OmitRepository omitRepo;
	
	@Autowired
	private PlantRepository plantRepo;
	
	@Autowired
	private UniprotRepository uniprotRepo;

	@Autowired
	private BaoRepository baoRepo;
	
	@Autowired
	private GeneralRepository generalRepo;
	
	@Autowired
	private CellosaurusRepository cellosaurusRepo;
	
	private GoConnector goConn;
	private OboNcitConnector oboNcitConn;
	private ChebiConnector chebiConn;
	private MondoConnector mondoConn;
	private FaoConnector faoConn;
	private CloConnector cloConn;
	private UberonConnector uberonConn;
	private OmitConnector omitConn;
	private PlantConnector plantConn;
	private NcbiTaxonConnector ncbiTaxonConn;
	private UniprotConnector uniprotConn;
	private BaoConnector baoConn;
	private CellosaurusConnector cellosaurusConn;
	
    private static final Logger logger = LoggerFactory.getLogger(OLSCallController.class);
	
    private Properties prop;
    
    /**
     * "Home" page...
     * @return
     */
	@RequestMapping("/")
    public String home() {
		return 
			"<html>\n" + 
			"<body>\n" + 
			"<h1>Ontology Lookup Service </h1>\n" + 
			"<h2>Fairness for Pharma Data </h2>\n" + 
			"The service currently offers the following methods\n" + 
			"<ul>\n" + 
			"	<li>suggest</li>\n" + 
			"	<li>getChildren</li>\n" +
			"   <li>getTree</li>\n" + 
			"	<li>update</li>\n" + 
			"</ul>\n" + 
			"The service currently queries the following ontologies\n" + 
			"<ul>\n" + 
			"	<li>go</li>\n" + 
			"	<li>ncit</li>\n" +
			"   <li>ncbitaxon</li>\n" + 
			"	<li>mondo</li>\n" +
			"	<li>fao</li>\n" + 
			"	<li>clo</li>\n" + 
			"	<li>uberon</li>\n" + 
			"	<li>plant</li>\n" + 
			"	<li>omit</li>\n" + 
			"	<li>chebi</li>\n" + 
			"	<li>uniprot</li>\n" + 
			"	<li>bao</li>\n" + 
			"	<li>cellosaurus</li>\n" + 
			"</ul>\n" +			
			"For more info please visit the <a href=\"https://ghe.phaidra.org/FAIRness/OLSService\">github page</a>.\n" + 
			"</body>\n" + 
			"</html>";
	}
	
    
	/**
     * Updates terms based on the list in application.properties
	 * 
	 **/
	@RequestMapping("/update")
    public String update() {
		
		logger.info("Update called.");
		
		this.prop = new Properties();
		try {
		    //load a properties file from class path, inside static method
			this.prop.load(Application.class.getClassLoader().getResourceAsStream("application.properties"));

		} 
		catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
		}
		

		try {
			
			goConn = new GoConnector(this.prop);
			oboNcitConn = new OboNcitConnector(this.prop);
			mondoConn = new MondoConnector(this.prop);
			faoConn = new FaoConnector(this.prop);
			cloConn = new CloConnector(this.prop);
			uberonConn = new UberonConnector(this.prop);
			omitConn = new OmitConnector(this.prop);
			plantConn = new PlantConnector(this.prop);
			ncbiTaxonConn = new NcbiTaxonConnector(this.prop);
			chebiConn = new ChebiConnector(this.prop);
			uniprotConn = new UniprotConnector(this.prop);
			baoConn = new BaoConnector(this.prop);
			cellosaurusConn = new CellosaurusConnector(this.prop);
			

			// all the root term IRI properties are prefixed with this
			//in application.preperties
			String prefix = "ontology";
			

			for (Map.Entry<Object, Object> entry : this.prop.entrySet()) {
		        String key = (String) entry.getKey();
		        String value = (String) entry.getValue();

		        if (key.startsWith(prefix) && !value.isEmpty()) {
		        	logger.debug("Found root term entry: " + key + " IRI: " + value);
		        	
		        	if(key.startsWith(prefix+".go")) {
		        		updateParentPath(goConn, value, goRepo, value);
		        	} else if(key.startsWith(prefix+".oboncit")) {
		        		updateParentPath(oboNcitConn, value, oboNcitRepo, value);
		        	} else if(key.startsWith(prefix+".mondo")) {
		        		updateParentPath(mondoConn, value, mondoRepo, value);
		        	} else if(key.startsWith(prefix+".fao")) {
		        		updateParentPath(faoConn, value, faoRepo, value);
					} else if(key.startsWith(prefix+".clo")) {
		        		updateParentPath(cloConn, value, cloRepo, value);
		        	} else if(key.startsWith(prefix+".uberon")) {
		        		updateParentPath(uberonConn, value, uberonRepo, value);
		        	} else if(key.startsWith(prefix+".plant")) {
		        		updateParentPath(plantConn, value, plantRepo, value);
		        	} else if(key.startsWith(prefix+".omit")) {
		        		updateParentPath(omitConn, value, omitRepo, value);
		        	} else if(key.startsWith(prefix+".ncbitaxon")) {
		        		updateParentPath(ncbiTaxonConn, value, ncbiTaxonRepo, value);
		        	} else if(key.startsWith(prefix+".chebi")) {
		        		updateParentPath(chebiConn, value, chebiRepo, value);
		        	} else if(key.startsWith(prefix+".uniprot")) {
		        		updateParentPath(uniprotConn, value, uniprotRepo, value);
		        	} else if(key.startsWith(prefix+".bao")) {
		        		updateParentPath(baoConn, value, baoRepo, value);
		        	} else if(key.startsWith(prefix+".cellosaurus")) {
		        		updateParentPath(cellosaurusConn, value, cellosaurusRepo, value);
		        	} 
		        	
		        	logger.info("Update done.");
		        }
		        
		    }
			
		} catch (Exception e) {

			logger.error(e.getMessage(), e);
			return "{ \"updateStatus\": \"failed\"}";
		}
			
		// Report Success.
		return "{ \"updateStatus\": \"success\"}";
    }	


	
	/**
	 * Runs the update in two steps
	 * first retrieves the terms and writes a list of parent-child relations
	 * second iterates the list and updates the terms with the parent-child relations
	 * this is done so because most of the ontologies allow multiple parents and multiple children for a term.
	 * @param esc
	 * @param classParentTerm
	 * @param repo
	 * @param ontoClass
	 * @throws ExternalServiceConnectorException
	 */
    public void updateParentPath(ExternalServiceConnector esc, String classParentTerm, Object repo, String ontoClass) throws ExternalServiceConnectorException {
    	
    	logger.debug(" Updating " + classParentTerm + " in " + ontoClass);
    	
    	if(classParentTerm.isEmpty() || classParentTerm == null) {
    		logger.warn("Class parent term is empty, exiting.");
    		return;
    	}
    	
		HashMap<String, String> urlsOTermParents = new HashMap<String, String>();
		
		try {
			
			esc.setRepo(repo);
		
			if(classParentTerm != null) {
				
				//strip the URL part, keep the IRI
				classParentTerm = classParentTerm.replace(this.prop.getProperty("purloboprefix"), "");
				
				//save class parent term first
				esc.saveOne(classParentTerm, ontoClass);
				esc.setIri(classParentTerm);
			}
			
			urlsOTermParents.putAll(esc.queryAndStoreOLS(ontoClass));
		
			for(Entry<String, String> entry : urlsOTermParents.entrySet()) {
				logger.debug("GetParentByURL: "+entry.getValue()+" - "+entry.getKey());
				esc.linkParents(entry.getValue(), entry.getKey());
			}
		
		
		} catch (ExternalServiceConnectorException e) {
			logger.info("Update failed.");
			throw new ExternalServiceConnectorException(e.getMessage());
		}
		// Report Success.
		logger.info("Update successful.");
		return;
    	
    }		
    

    /**
     * Helper to get the correct repo implementation
     * Is a survivor from a previous concept where the ontologies 
     * has severe differences and such distinction seemed logical
     * @param ontology
     * @return
     */
    private AbstractRepository getRepoImpl(String ontology) {
    	switch(ontology) {
    	case "go":
    		return goRepo;
    	case "ncit":
    		return oboNcitRepo;
    	case "ncbitaxon":
    		return ncbiTaxonRepo;
    	case "mondo":
    		return mondoRepo;
    	case "fao":
    		return faoRepo;
	    case "clo":
    		return cloRepo;
    	case "uberon":
    		return uberonRepo;
    	case "plant":
    		return plantRepo;
    	case "omit":
    		return omitRepo;
    	case "chebi":
    		return chebiRepo;
    	case "uniprot":
    		return uniprotRepo;
    	case "bao":
    		return baoRepo;   		
    	case "cellosaurus":
    		return cellosaurusRepo;    		
    	case "general":
    		return generalRepo;
    	default:
    		return null;
    	}   	
    }
    
    /**
     * Filters a list to make sure the IRIs are unique in it
     * helper class to requests where one term can appear multiple times
     * because it has multiple parents.
     * @param terms
     * @return
     */
	private List<AbstractTerm> makeDistinctByIri(List<AbstractTerm> terms) {
		List<String> iriList = new ArrayList<String>();
		
		List<AbstractTerm> distinctList = new ArrayList<AbstractTerm>();
		
		for(AbstractTerm term : terms) {
			if(!iriList.contains(term.getIri())) {
				iriList.add(term.getIri());
				distinctList.add(term);
			}			
		}
		
		return distinctList;
	}
    
    /**
     * gets all the children down the
     * tree in a nested form
     * @param parent
     * @param ontology
     * @param ontoClass
     * @return
     */
    @RequestMapping("/gettree")
    public String getTree(
    		@RequestParam(value="parent", defaultValue="C60743") String parent,
    		@RequestParam(value="ontology", defaultValue="ncit") String ontology,
    		@RequestParam(value="class", defaultValue="") String ontoClass,
    		@RequestParam(value="filter", defaultValue="") String filter){
  	
    	AbstractRepository repo = getRepoImpl(ontology);
    	if(repo == null) {
    		logger.warn("Ontology "+ontology+" not supported.");
    		return new JSONArray("error").toString();
    	}
    	
    	
		
		Properties prop = new Properties();
		try {
		    //load a properties file from class path, inside static method
		    prop.load(Application.class.getClassLoader().getResourceAsStream("application.properties"));

		} 
		catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
		}
		
		
    	Driver driver = GraphDatabase.driver( prop.getProperty("spring.data.neo4j.uri"), AuthTokens.basic( 
    			prop.getProperty("spring.data.neo4j.username"), 
    			prop.getProperty("spring.data.neo4j.password") ) 
    			);
    	Session session = driver.session();

    	StatementResult result;
    	
    	// it is running a raw Cypher query, because the result is always one "record" 
    	// reason: the query uses the APOC plugin to convert the results into a tree
    	if(filter.isEmpty()) {
    		result = session.run( "MATCH p=(n:AbstractTerm)<-[:CHILD*]-(m) WHERE n.iri CONTAINS '"+parent+"' AND lower(n.ontoclass) CONTAINS lower('"+ontoClass+"') WITH COLLECT(p) AS ps CALL apoc.convert.toTree(ps) yield value RETURN value as getTreeResult;" );
    	} else {
    		result = session.run( "MATCH p=(n:AbstractTerm)<-[:CHILD*]-(m) WHERE n.iri CONTAINS '"+parent+"' AND lower(n.ontoclass) CONTAINS lower('"+ontoClass+"') AND m.iri CONTAINS '"+filter+"' WITH COLLECT(p) AS ps CALL apoc.convert.toTree(ps) yield value RETURN value as getTreeResult;" );
    	}
    	
    	// since the parent as a root can only have one tree
    	// there will have always only 1 result.
	    Record record = result.next();
	    JSONObject resultObject = new JSONObject(record.asMap());
	    
    	session.close();
    	driver.close();    	
    	
    	return resultObject.toString();

    }
    
    
	
	/**
	 * Returns the terms that have the parent specified in the parameter
	 * Uses the CHILD relation
	 * 
     * @param parent
     * @param ontology
     * @param ontoClass
	 * @return
	 */
    @RequestMapping("/getchildren")
    public String getChildren(
    		@RequestParam(value="parent", defaultValue="C60743") String parent,
    		@RequestParam(value="ontology", defaultValue="ncit") String ontology,
    		@RequestParam(value="class", defaultValue="") String ontoClass) {
    	
    	JSONObject returnObject = new JSONObject();
    	JSONArray childrenArray = new JSONArray();    	
    	
    	AbstractRepository repo = getRepoImpl(ontology);
    	if(repo == null) {
    		logger.warn("Ontology "+ontology+" not supported.");
    		return "{error: \"Ontology "+ontology+" not supported.\"}";
    	}
    	
		List<AbstractTerm> children = new ArrayList<AbstractTerm>();
		
		if(ontoClass.isEmpty()) {
			children = repo.findByParent(parent);
		} else {
			children = repo.findByParent(parent, ontoClass);
		}

		for(AbstractTerm t : children) {
			
			boolean notDuplicate = true;
			
			//duplicate check #20758
			for(Object child: childrenArray) {
				JSONObject jsonObjChild = (JSONObject) child;
				JSONArray jsonObjChildExactmatchArr = (JSONArray) jsonObjChild.get("skos:exactMatch");
				if(jsonObjChildExactmatchArr.get(0).toString().equals(t.getIri())) {
					notDuplicate = false;
				}
				
			}
			
			if(notDuplicate) {
				childrenArray.put(t.toJSON());
			}
			
		}   	
    	
    	returnObject.put("getChildrenResult", childrenArray);
    	
    	return returnObject.toString();
    	
    }
	
    
    
    
    /**
     * Helper method to check whether a term is in the DB or not
     * @param iri
     * @param ontology
     * @param ontoClass
     * @return
     */
    @RequestMapping("/checkiri")
    public String checkIri(
    		@RequestParam(value="iri", defaultValue="") String iri,
    		@RequestParam(value="ontology", defaultValue="") String ontology,
    		@RequestParam(value="class", defaultValue="") String ontoClass) {
    
    	AbstractRepository repo = getRepoImpl(ontology);
    	if(repo == null) {
    		logger.warn("Ontology "+ontology+" not supported.");
    		return "{error: \"Ontology "+ontology+" not supported.\"}";
    	}
    	
    	List<AbstractTerm> hits = new ArrayList<AbstractTerm>();
    	JSONObject resp = new JSONObject();
    	
		hits = repo.findByIri(iri);
		logger.warn(hits.toString()+" "+hits.size());
		
		if (hits.isEmpty())
			resp.put("response", false);
		else
			resp.put("response", true);
		
		return resp.toString();
    	
    }
    
    /**
     * Fetches data of a saved term by its IRI
     * this is supposed to follow the suggest call, where
     * we get the IRI-s of the matching terms.
     * 
     * This is a modification requested early december of 2019
     * to handle the meanwhile much more property-richer terms
     * 
     * @param iri
     * @param ontology
     * @param ontoClass
     * @return
     */
	@RequestMapping("/getdata")
    public String getData(
    		@RequestParam(value="iri", defaultValue="") String iri	
    		) {
		
		if(iri.isEmpty())
			return null;
		
		AbstractRepository repo = getRepoImpl("general");
		
		try {
		
		AbstractTerm term = repo.getTermbyIri(iri);
		
		return term.toJSON().toString();
		
		} catch (NullPointerException npe) {
			return "{ \"result\": \"empty\"}";
		}
		
		
		
	}
    
    /**
     * 
     * WHAT IT DOES:
     * - query persisted data
     * - query by text (i.e. you begin to type "cor" and both CORvus and uniCORn show up...
     * - display results
     * @param iri
     * @return
     */
	@RequestMapping("/suggest")
    public String suggest(
    		@RequestParam(value="label", defaultValue="extra") String label,
    		@RequestParam(value="ontology", defaultValue="go") String ontology,
    		@RequestParam(value="class", defaultValue="") String ontClass,
			@RequestParam(value="limit", defaultValue="") String limit) {      

    	if(label.length() < 3) {
    		logger.warn("Query label shorter than 3 characters, skipping.");
    		return "{error: \"Label too short!\"}";
    	}
		
    	JSONObject returnObject = new JSONObject();
    	JSONArray suggestArray = new JSONArray();
		   	
    	AbstractRepository repo = getRepoImpl(ontology);
    	if(repo == null) {
    		logger.warn("Ontology "+ontology+" not supported.");
    		return "{error: \"Ontology "+ontology+" not supported.\"}";
    	}	
		
    	logger.debug("Suggest called: " + label + " / "+ ontology + " / " + ontClass);
    	
    	// build the context object here, then add the hits as array elements below
    	returnObject.put("@context", ontology);
    	if(!ontClass.isEmpty())
			returnObject.put("class", ontClass);
    	
    	List<AbstractTerm> hits = new ArrayList<AbstractTerm>();
    	
    	if(!ontClass.isEmpty())
    		hits = repo.findBySynonym(label, ontClass);
    	else
    		hits = repo.findBySynonym(label);
    	
    	// Bug #20535 fix
    	List<AbstractTerm> uniqueHits = this.makeDistinctByIri(hits);
    	
		for(AbstractTerm t : uniqueHits) {
			suggestArray.put(t.toIriLabelJSON());
			logger.debug("Suggest hit: " + t.getIri());
		}    	
    	
    	returnObject.put(label, suggestArray);
    	
    	return returnObject.toString();
    	
    }
	

	
}

